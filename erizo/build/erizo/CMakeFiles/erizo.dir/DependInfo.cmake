# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/root/licode/erizo/src/erizo/dtls/bf_dwrap.c" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/dtls/bf_dwrap.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/licode/erizo/src/erizo/DtlsTransport.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/DtlsTransport.cpp.o"
  "/root/licode/erizo/src/erizo/NiceConnection.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/NiceConnection.cpp.o"
  "/root/licode/erizo/src/erizo/OneToManyProcessor.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/OneToManyProcessor.cpp.o"
  "/root/licode/erizo/src/erizo/SdpInfo.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/SdpInfo.cpp.o"
  "/root/licode/erizo/src/erizo/SrtpChannel.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/SrtpChannel.cpp.o"
  "/root/licode/erizo/src/erizo/Stats.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/Stats.cpp.o"
  "/root/licode/erizo/src/erizo/StringUtil.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/StringUtil.cpp.o"
  "/root/licode/erizo/src/erizo/WebRtcConnection.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/WebRtcConnection.cpp.o"
  "/root/licode/erizo/src/erizo/dtls/DtlsClient.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/dtls/DtlsClient.cpp.o"
  "/root/licode/erizo/src/erizo/dtls/DtlsFactory.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/dtls/DtlsFactory.cpp.o"
  "/root/licode/erizo/src/erizo/dtls/DtlsSocket.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/dtls/DtlsSocket.cpp.o"
  "/root/licode/erizo/src/erizo/dtls/DtlsTimer.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/dtls/DtlsTimer.cpp.o"
  "/root/licode/erizo/src/erizo/dtls/OpenSSLInit.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/dtls/OpenSSLInit.cpp.o"
  "/root/licode/erizo/src/erizo/media/ExternalInput.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/media/ExternalInput.cpp.o"
  "/root/licode/erizo/src/erizo/media/ExternalOutput.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/media/ExternalOutput.cpp.o"
  "/root/licode/erizo/src/erizo/media/MediaProcessor.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/media/MediaProcessor.cpp.o"
  "/root/licode/erizo/src/erizo/media/OneToManyTranscoder.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/media/OneToManyTranscoder.cpp.o"
  "/root/licode/erizo/src/erizo/media/codecs/AudioCodec.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/media/codecs/AudioCodec.cpp.o"
  "/root/licode/erizo/src/erizo/media/codecs/VideoCodec.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/media/codecs/VideoCodec.cpp.o"
  "/root/licode/erizo/src/erizo/media/mixers/VideoMixer.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/media/mixers/VideoMixer.cpp.o"
  "/root/licode/erizo/src/erizo/media/mixers/VideoUtils.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/media/mixers/VideoUtils.cpp.o"
  "/root/licode/erizo/src/erizo/rtp/RtpPacketQueue.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/rtp/RtpPacketQueue.cpp.o"
  "/root/licode/erizo/src/erizo/rtp/RtpSink.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/rtp/RtpSink.cpp.o"
  "/root/licode/erizo/src/erizo/rtp/RtpSource.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/rtp/RtpSource.cpp.o"
  "/root/licode/erizo/src/erizo/rtp/RtpVP8Fragmenter.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/rtp/RtpVP8Fragmenter.cpp.o"
  "/root/licode/erizo/src/erizo/rtp/RtpVP8Parser.cpp" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/rtp/RtpVP8Parser.cpp.o"
  "/root/licode/erizo/src/erizo/rtp/webrtc/fec_receiver_impl.cc" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/rtp/webrtc/fec_receiver_impl.cc.o"
  "/root/licode/erizo/src/erizo/rtp/webrtc/forward_error_correction.cc" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/rtp/webrtc/forward_error_correction.cc.o"
  "/root/licode/erizo/src/erizo/rtp/webrtc/forward_error_correction_internal.cc" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/rtp/webrtc/forward_error_correction_internal.cc.o"
  "/root/licode/erizo/src/erizo/rtp/webrtc/rtp_utility.cc" "/root/licode/erizo/build/erizo/CMakeFiles/erizo.dir/rtp/webrtc/rtp_utility.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/root/licode/erizo/src/erizo"
  "/root/licode/erizo/src/../../build/libdeps/build/include"
  "/usr/include/glib-2.0"
  "/usr/lib/arm-linux-gnueabihf/glib-2.0/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
